package com.example.testcamapp.Data

data class RegistrarData (
    val name: String?,
    val media_url: String?
)
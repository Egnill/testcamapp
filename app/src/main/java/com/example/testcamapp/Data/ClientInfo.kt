package com.example.testcamapp.Data

import com.google.gson.annotations.SerializedName

data class ClientInfo (
        @SerializedName("nChannel")
        val nChannel: Int?, //Channel NO.
        @SerializedName("nStream")
        val nStream: Int?, //0: main stream，1: extra stream
        @SerializedName("nMode")
        val nMode: Int?, //0：TCP, 1：UDP
        @SerializedName("nComType")
        val nComType: Int? //only efficient for combination encode，for jigsaw module
)
package com.example.testcamapp.Utils

import java.net.InetAddress
import java.net.Socket

class CamSocket(private var host: String, private var port: Int) {

    /*private var host = "172.17.8.67" //172.17.8.67
    private var port = "34567"*/

    private var socket: Socket? = null

    fun getSocket(): Socket? = socket

    /*init {
        *//*host = "172.17.8.67"
        port = "34567"// TCP*//*
        socket = Socket(host, port.toInt())
    }*/

    fun onConnection() {
        val serverAddr: InetAddress = InetAddress.getByName(host)
        socket = Socket(serverAddr, port.toInt())
    }

    fun onDisconnect() {
        if (socket!!.isConnected) {
            socket!!.close()
        }
    }

    interface CamSocketListener {
        fun onStart()
        fun onStop()
    }
}
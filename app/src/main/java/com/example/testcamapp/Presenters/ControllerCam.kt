package com.example.testcamapp.Presenters

import android.util.Log
import androidx.annotation.RestrictTo
import androidx.lifecycle.ViewModel
import com.example.testcamapp.Data.ClientInfo
import com.example.testcamapp.Utils.CamSocket
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.*
import java.net.Socket
import kotlin.concurrent.thread

class ControllerCam : ViewModel() {

    private var input: BufferedReader? = null
    private var output: PrintWriter? = null
    private var socket: CamSocket? = null

    private var name = "admin"
    private var password = "1q2w3e4r"

    /*init {
        socket = CamSocket("172.17.8.67", 34567)
        input = BufferedReader(InputStreamReader(socket!!.getSocket()?.getInputStream()))
        output = PrintWriter(BufferedWriter(OutputStreamWriter(socket!!.getSocket()?.getOutputStream())))
    }*/

    fun startStream() {
        /*socket = CamSocket("172.17.8.67", 34567)
        input = BufferedReader(InputStreamReader(socket!!.getSocket()?.getInputStream()))
        output = PrintWriter(BufferedWriter(OutputStreamWriter(socket!!.getSocket()?.getOutputStream())))*/
        _startStreamBackgrauund()
        /*val clientInfo = ClientInfo(
                nChannel = null,
                nStream = 0,
                nMode = 0,
                nComType = null
        )
        output!!.println(clientInfo)*/
    }

    private fun _startStreamBackgrauund() = runBlocking{
        launch(Dispatchers.IO) {
            socket = CamSocket("172.17.8.67", 34567)
            socket!!.onConnection()
            input = BufferedReader(InputStreamReader(socket!!.getSocket()?.getInputStream()))
            output = PrintWriter(BufferedWriter(OutputStreamWriter(socket!!.getSocket()?.getOutputStream())))
            if (socket!!.getSocket()!!.isConnected) {
                Log.d("Connection", "Successful")
            } else {
                Log.d("Connection", "Fail")
            }
            val clientInfo = ClientInfo(
                    nChannel = null,
                    nStream = 0,
                    nMode = 0,
                    nComType = null
            )
            //output!!.println(clientInfo.toString())
            Log.d("Socket", input!!.readLine())
        }
    }
}
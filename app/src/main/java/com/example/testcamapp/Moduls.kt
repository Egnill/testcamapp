package com.example.testcamapp

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.ViewModel
import com.example.testcamapp.Presenters.ControllerCam
import com.example.testcamapp.Utils.CamSocket
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

val viewModules: Module = module {
    viewModel { ControllerCam() }
}

val sharedPreferences = module {
    single {
        getSharedPrefs(get())
    }
}

val MODULES = listOf(viewModules, sharedPreferences)

private fun getSharedPrefs(context: Context): SharedPreferences {
    return context.getSharedPreferences("default", Context.MODE_PRIVATE)
}
package com.example.testcamapp.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.testcamapp.Data.RegistrarData
import com.example.testcamapp.R

class ListCamsAdapter(private val items: List<RegistrarData>) : RecyclerView.Adapter<ListCamsAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_reg_cam, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = items[position]

        holder.itemView.findViewById<TextView>(R.id.reg_cam_label).text = item.name
        holder.itemView.setOnClickListener {
            holder.itemView.findNavController().navigate(R.id.action_RegistrarFragment_to_CamFragment)
        }
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
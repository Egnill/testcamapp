package com.example.testcamapp.windows

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.testcamapp.Adapters.ListRegistrarsAdapter
import com.example.testcamapp.Data.RegistrarData
import com.example.testcamapp.R

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class MainFragment : Fragment() {

    private val list_temp = ArrayList<RegistrarData>()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (list_temp.isEmpty()) {
            list_temp.add(RegistrarData(
                    name = "Reg1",
                    media_url = ""
            ))
        }

        view.findViewById<RecyclerView>(R.id.list_registrars).apply {
            adapter = ListRegistrarsAdapter(list_temp)
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        }
        /*view.findViewById<Button>(R.id.button_first).setOnClickListener {
            findNavController().navigate(R.id.action_MainFragment_to_RegistrarsFragment)
        }*/
    }
}
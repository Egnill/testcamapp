package com.example.testcamapp.windows

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.testcamapp.Adapters.ListCamsAdapter
import com.example.testcamapp.Data.RegistrarData
import com.example.testcamapp.R

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class RegistrarFragment : BaseFragment() {

    private var list_temp = ArrayList<RegistrarData>()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (list_temp.isEmpty()) {
            list_temp.add(RegistrarData(
                    name = "Cam1",
                    media_url = ""
            ))
            list_temp.add(RegistrarData(
                    name = "Cam2",
                    media_url = ""
            ))
        }

        view.findViewById<RecyclerView>(R.id.list_cams).apply {
            adapter = ListCamsAdapter(list_temp)
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        }
    }
}
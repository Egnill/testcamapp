package com.example.testcamapp.windows

import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.VideoView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.testcamapp.Presenters.ControllerCam
import com.example.testcamapp.R
import com.rvirin.onvif.onvifcamera.*
import org.json.JSONObject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.io.BufferedReader
import java.io.PrintWriter
import java.net.Socket

class CamFragment : BaseFragment(), OnvifListener {

    private val camController: ControllerCam by sharedViewModel()
    private var socket: Socket? = null
    private var input: BufferedReader? = null
    private var output: PrintWriter? = null
    private lateinit var video_frame: VideoView
    private lateinit var surfaceView: SurfaceView

    private var name = "admin"
    private var password = "1q2w3e4r"
    private var uri = "rtsp://172.17.8.67/"

    private val TAG = "Socket"

    private external fun H264DVRLogin() : String
    private external fun H264DVRLogout() : String

    init {
        //System.loadLibrary("InterlayerInterface")
        //System.loadLibrary("libxmnetsdk")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cam, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //System.loadLibrary("native-lib")
        //view?.findViewById<TextView>(R.id.title)!!.text = stringFromJNINew()
        /*runBlocking {
            launch(Dispatchers.IO) {
                var i = 0
                socket = Socket(InetAddress.getByName("172.17.8.67"), 34567)
                input = BufferedReader(InputStreamReader(socket!!.getInputStream()))
                output = PrintWriter(BufferedWriter(OutputStreamWriter(socket!!.getOutputStream())))
                if (socket!!.isConnected) {
                    Log.d("Connection", "Successful")
                } else {
                    Log.d("Connection", "Fail")
                }

                val clientInfo = ClientInfo(
                        nChannel = null,
                        nStream = 0,
                        nMode = 0,
                        nComType = null
                )

                var gson = GsonBuilder().create()
                Log.d(TAG, gson.toJson(clientInfo))
                output!!.println(gson.toJson(clientInfo))
                //Log.d(TAG, input!!.readLine())
            }
            *//*while (true) {
                Log.d("Socket", input!!.readLine())
                i++
            }*//*
        }*/
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //currentDevice = OnvifDevice("172.17.8.76", "admin", "")

        /*currentDevice.rtspURI = "rtsp://172.17.8.67/channel=1"
        currentDevice.listener = this
        currentDevice.getServices()*/

        /*surfaceView = view.findViewById(R.id.surfaceView);
        try {
            //var mp = MediaPlayer()
            var mp = MediaPlayer.create(this.context, Uri.parse("rtsp://${name}:${password}@172.17.8.67/channel=1"), surfaceView.holder)
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }*/
        //mp.start()
        /*video_frame = view.findViewById(R.id.video_frame);
        video_frame.setVideoURI(Uri.parse("rtsp://172.17.8.67/channel=1"));
        video_frame.setMediaController(MediaController(this.context));
        video_frame.requestFocus();
        video_frame.start();*/
        //Log.d("Cam", H264DVRLogin())
    }

    override fun requestPerformed(response: OnvifResponse) {
        Log.d("INFO", response.parsingUIMessage)

        //Log.d(TAG, H264DVRLogin())
    }

    /*public class MediaPlayerActivity: AppCompatActivity(), SurfaceHolder.Callback {

        override fun surfaceCreated(holder: SurfaceHolder) {
            try {
                mediaPlayer = MediaPlayer()
                mediaPlayer.setDisplay(videoHolder)
                mediaPlayer.setDataSource(videoAddress)
                mediaPlayer.setOnPreparedListener(this)
                mediaPlayer.prepareAsync()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
            TODO("Not yet implemented")
        }

        override fun surfaceDestroyed(holder: SurfaceHolder) {
            TODO("Not yet implemented")
        }
    }*/
}